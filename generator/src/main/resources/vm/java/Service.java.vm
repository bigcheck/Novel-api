package ${package}.service;

import ${package}.domain.${className};
import java.util.List;

/**
 * ${tableComment} 服务层
 * 
 * @author ${author}
 * @date ${datetime}
 */
public interface ${className}Service {
	
	/**
     * 查询${tableComment}信息
     * 
     * @param ${primaryKey.attrname} ${tableComment}ID
     * @return ${tableComment}信息
     */
	${className} select${className}ById(${primaryKey.attrType} ${primaryKey.attrname});
	
	/**
     * 查询${tableComment}列表
     * 
     * @param ${classname} ${tableComment}信息
     * @return ${tableComment}集合
     */
	List<${className}> select${className}List(${className} ${classname});
	
	/**
     * 新增${tableComment}
     * 
     * @param ${classname} ${tableComment}信息
     * @return 结果
     */
	boolean insert${className}(${className} ${classname});
	
	/**
     * 修改${tableComment}
     * 
     * @param ${classname} ${tableComment}信息
     * @return 结果
     */
	boolean update${className}(${className} ${classname});
	
	/**
     * 保存${tableComment}
     * 
     * @param ${classname} ${tableComment}信息
     * @return 结果
     */
	boolean save${className}(${className} ${classname});
	
	/**
     * 删除${tableComment}信息
     * 
     * @param ${primaryKey.attrname} ${tableComment}ID
     * @return 结果
     */
	boolean delete${className}ById(${primaryKey.attrType} ${primaryKey.attrname});
	
	/**
     * 批量删除${tableComment}信息
     * 
     * @param ${primaryKey.attrname}s 需要删除的数据ID
     * @return 结果
     */
	boolean batchDelete${className}(${primaryKey.attrType}[] ${primaryKey.attrname}s);
	
}
